module SSIndexingTools



"""
function NodesPerDimension(m::Int64,p::Int64)
==================
# Parameters:
* m: Number of elements (dimension-direction)
* p: Polynomial degree (dimension-direction)

# Returns:
* Returns: The number of nodes wide (or long)
"""
function NodesPerDimension(m::Int64,p::Int64)
    return (m+1)+m*(p-1)
end




"""
function KroneckerDelta(i::Int64,j::Int64)
=======

# Returns:
* The value of the Kronecker Delta function given i,j
"""
function KroneckerDelta(i::Int64,j::Int64)
    return (i==j)
end





"""
function IndexForRowCol(i,j,nrows)
=======

# Returns:
* 1-based index for the 1-based row,col indicated (column-major order)
"""
function IndexForRowCol(i,j,nrows)
    return (j-1)*nrows + i
end






"""
function RowColForIndex(i,nrows)
=========

# Returns:
* 1-based row,col for the 1-based index indicated (column-major order)
"""
function RowColForIndex(i,nrows)
    row = ((i-1) % nrows)+1
    col = div(i-1,nrows)+1
    return row, col
end




"""
function LagrangeIEN(e::Int64,p::Int64)
==================
For 1D.

# Parameters:
* e: The element index (first index is 1)
* p: Polynomial degree (x-direction)

# Returns:
* Returns: Array containing the indices of
           each node in the specified element
"""
function LagrangeIEN(e::Int64,p::Int64)

    # Number of nodes per element
    nodes_per_element = NodesPerDimension(1,p)

    # Initialize the array
    IEN = zeros( Int64, nodes_per_element )

    node_i = (e-1)*(nodes_per_element-1)+1
    for i = 1:nodes_per_element
        IEN[i] = node_i
        node_i += 1
    end

    return IEN
end



"""
function IsogeometricIEN(e::Int64,p::Int64)
==================
For 1D. Returns the indicies of the IGA basis functions
associated with element e.

# Parameters:
* e: The element index (first index is 1)
* p: Polynomial degree (x-direction)

# Returns:
* Returns: Array containing the indices of
           each node in the specified element
"""
function IsogeometricIEN(e::Int64,p::Int64)

    # Number of nodes per element
    nodes_per_element = NodesPerDimension(1,p)

    # Initialize the array
    IEN = zeros( Int64, nodes_per_element )

    node_i = e
    for i = 1:nodes_per_element
        IEN[i] = node_i
        node_i += 1
    end

    return IEN
end



end # module SSIndexingTools










#=

"""
function LagrangeIEN(e::Int64,nelements::Array{Int64},degree::Array{Int64})
=============

# Parameters
* e: The element index (first index is 1)
* nelements::Array{Int64} Number of elements in each parameter direction
* degree::Array{Int64} Polynomial degree in each parameter direction

# Returns:
* Array containing the indices of each node in the specified element
"""
function LagrangeIEN(e::Int64,nelements::Array{Int64},degree::Array{Int64})

    # TODO: Make this a multi-d function (works for dim_p = 1d, 2d, and 3d)
    # for now it only works for dim_p = 2d
    m = nelements[1]
    p = degree[1]
    q = degree[2]

    # Number of nodes per row
    nodes_in_x = (m+1)+m*(p-1)

    # element x_index, y_index
    e_x_index, e_y_index = RowColForIndex(e,m)

    # node row, col minimum and maximum
    node_xmin = ((e_x_index-1) + (e_x_index-1)*(p-1)) + 1
    node_xmax = (e_x_index + e_x_index*(p-1)) + 1
    node_ymin = ((e_y_index-1) + (e_y_index-1)*(q-1)) + 1
    node_ymax = (e_y_index + e_y_index*(q-1)) + 1

    #Result is an array the length of the number of nodes in the element
    elnum = 1
    BBIEN = zeros( Int64, (node_xmax-node_xmin+1)*(node_ymax-node_ymin+1) )
    for j=node_ymin:node_ymax
        for i=node_xmin:node_xmax
            node_index = IndexForRowCol(i,j,nodes_in_x)
            BBIEN[elnum] = convert(Int64,floor(node_index))
            elnum += 1
        end
    end

    return BBIEN
end



"""
function IsogeometricIEN(e::Int64,nelements::Array{Int64},degree::Array{Int64})
==================
For 2D.
"""
function IsogeometricIEN(e::Int64,nelements::Array{Int64},degree::Array{Int64})
    # TODO: Make this a multi-d function (for now, only for 2D)

    m = nelements[1]
    p = degree[1]
    q = degree[2]

    # Number of nodes per row
    nodes_in_x = nelements[1] + p

    # element x_index, y_index
    e_x_index, e_y_index = RowColForIndex(e,m)

    node_xmin = e_x_index
    node_xmax = e_x_index + p
    node_ymin = e_y_index
    node_ymax = e_y_index + p

    elnum = 1
    IEN = zeros( Int64, (node_xmax-node_xmin+1)*(node_ymax-node_ymin+1) )
    for j=node_ymin:node_ymax
        for i=node_xmin:node_xmax
            node_index = IndexForRowCol(i,j,nodes_in_x)
            IEN[elnum] = convert(Int64,floor(node_index))
            elnum += 1
        end
    end

    return IEN
end

=#
